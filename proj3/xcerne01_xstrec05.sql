-- IDS PROJECT 3
-- 9.4.2015
-- xcerne01@stud.fit.vutbr.cz
-- xstrec05@stud.fit.vutbr.cz


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Zrusenie tabuliek a sekvencii

DROP TABLE Lekar CASCADE CONSTRAINTS;
DROP TABLE Vysetrenie CASCADE CONSTRAINTS;
DROP TABLE Sestra CASCADE CONSTRAINTS;
DROP TABLE Oddelenie CASCADE CONSTRAINTS;
DROP TABLE Detail_o_sluzbe CASCADE CONSTRAINTS;
DROP TABLE Uvazok CASCADE CONSTRAINTS;
DROP TABLE Hospitalizacia CASCADE CONSTRAINTS;
DROP TABLE Naordinovany_Liek CASCADE CONSTRAINTS;
DROP TABLE Liek CASCADE CONSTRAINTS;
DROP TABLE Pacient CASCADE CONSTRAINTS;
DROP SEQUENCE seq_id_vysetrenie;
DROP SEQUENCE seq_id_lekar;
DROP SEQUENCE seq_id_sestra;
DROP SEQUENCE seq_id_hospitalizacia;
DROP SEQUENCE seq_id_liek;
DROP SEQUENCE seq_id_naordinovany_liek;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Vytvorenie tabuliek

CREATE TABLE Lekar (
    id                  INTEGER         PRIMARY KEY,
    priezvisko          VARCHAR2(20)    NOT NULL,
    meno                VARCHAR2(20)    NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    ulica               VARCHAR2(20)    NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               VARCHAR2(40)    NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE,
    titul               VARCHAR2(10),
    kvalifikacia        VARCHAR2(100)
);

CREATE TABLE Oddelenie (
    skratka             CHAR(3)         PRIMARY KEY,
    nazov               VARCHAR2(20)    NOT NULL,
    poschodie           INTEGER,
    telefon             CHAR(3)         UNIQUE   -- Rychla volba, preto len 3 znaky
);

CREATE TABLE Sestra (
    id                  INTEGER         PRIMARY KEY,
    priezvisko          NVARCHAR2(20)   NOT NULL,
    meno                NVARCHAR2(20)   NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    ulica               NVARCHAR2(20)   NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               NVARCHAR2(40)   NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE,
    datum_preskolenia   DATE,
    id_oddelenie        CHAR(3)         NOT NULL
);

CREATE TABLE Uvazok (
    skratka             CHAR(3)         PRIMARY KEY,
    nazov               NVARCHAR2(20)   NOT NULL
);

CREATE TABLE Detail_o_sluzbe (
    id_lekar            INTEGER         NOT NULL,
    skratka_oddelenie   CHAR(3)         NOT NULL,
    skratka_uvazok      CHAR(3)         NOT NULL,
    zaciatok_sluzby     DATE            NOT NULL,
    koniec_sluzby       DATE
);

CREATE TABLE Pacient (
    rc                  CHAR(10)        PRIMARY KEY,
    priezvisko          NVARCHAR2(30)   NOT NULL,
    meno                NVARCHAR2(30)   NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    zdrav_poistovna     NVARCHAR2(30)   NOT NULL,
    ulica               NVARCHAR2(20)   NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               NVARCHAR2(40)   NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE
);

CREATE TABLE Hospitalizacia (
    id                  INTEGER         PRIMARY KEY,
    rc_pacient          CHAR(10)        NOT NULL,
    id_lekar            INTEGER         NOT NULL,
    zaciatok            DATE            NOT NULL,
    koniec              DATE,
    id_oddelenie        CHAR(3)         NOT NULL,
    cislo_izby          INTEGER         NOT NULL,
    diagnoza            NVARCHAR2(100),
    spec_poziadavky     NVARCHAR2(100)
);

CREATE TABLE Vysetrenie (
    id                  INTEGER         PRIMARY KEY,
    nazov               NVARCHAR2(50)   NOT NULL,
    datum               DATE            NOT NULL,
    id_hospitalizacia   INTEGER         NOT NULL,
    id_lekar            INTEGER         NOT NULL,
    skratka_oddelenie   CHAR(3)         NOT NULL,
    detaily             CLOB
);

CREATE TABLE Liek (
    id                  INTEGER         PRIMARY KEY,
    nazov               NVARCHAR2(30)   NOT NULL,
    forma               NVARCHAR2(50),
    ucinna_latka        NVARCHAR2(50),
    zlozenie            NVARCHAR2(255),
    indikacie           NVARCHAR2(255),
    kontraindikacie     NVARCHAR2(255),
    odporuc_davkovanie  NVARCHAR2(100)
);

CREATE TABLE Naordinovany_Liek (
    id                  INTEGER         PRIMARY KEY,
    id_liek             INTEGER         NOT NULL,
    id_hospitalizacia   INTEGER         NOT NULL,
    zaciatok_uzivania   DATE            NOT NULL,
    davkovanie          NVARCHAR2(50),
    mnozstvo            NVARCHAR2(50),
    specifikacie        NVARCHAR2(255)
);

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Pridanie integritnych omedzeni a checkov

ALTER TABLE Lekar ADD (
    CONSTRAINT CH_lekar_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_lekar_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_lekar_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Oddelenie ADD (
    CONSTRAINT CH_oddelenie_poschodie
        CHECK (poschodie >= 0),
    CONSTRAINT CH_oddelenie_telefon
        CHECK (REGEXP_LIKE(telefon,'[0-9]{3}'))
);

ALTER TABLE Sestra ADD (
    CONSTRAINT FK_sestra_oddelenie
        FOREIGN KEY (id_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT CH_sestra_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_sestra_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_sestra_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Detail_o_sluzbe ADD (
    CONSTRAINT PK_detaily_lekar_oddelenie 
        PRIMARY KEY (id_lekar, skratka_oddelenie),
    CONSTRAINT FK_detaily_lekar 
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT FK_detaily_oddelenie 
        FOREIGN KEY (skratka_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT FK_detaily_uvazok
        FOREIGN KEY (skratka_uvazok) 
        REFERENCES Uvazok
);

ALTER TABLE Pacient ADD (
    CONSTRAINT CH_pacient_rc
        CHECK (REGEXP_LIKE(rc,'[0-9]{2}(0[1-9]|1[0-2]|2[1-9]|3[0-2]|5[1-9]|6[0-2]|7[1-9]|8[0-2])0[1-9]|2[0-9]|3[01][0-9]{4}') AND MOD(rc,11) = 0),
    CONSTRAINT CH_pacient_zdrav_poistovna
        CHECK (zdrav_poistovna in ('Dôvera','Union','VŠZP')),
    CONSTRAINT CH_pacient_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_pacient_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_pacient_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Hospitalizacia ADD (
    CONSTRAINT FK_hospitalizacia_pacient_rc 
        FOREIGN KEY (rc_pacient) 
        REFERENCES Pacient,
    CONSTRAINT FK_hospitalizacia_oddelenie 
        FOREIGN KEY (id_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT FK_hospitalizacia_lekar
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT CH_hospitalizacia_cislo_izby
        CHECK (cislo_izby > 0)
);

ALTER TABLE Vysetrenie ADD (
    CONSTRAINT FK_vysetrenie_oddelenie
        FOREIGN KEY (skratka_oddelenie)
        REFERENCES Oddelenie,
    CONSTRAINT FK_vysetrenie_lekar
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT FK_vysetrenie_hospitalizacia
        FOREIGN KEY (id_hospitalizacia) 
        REFERENCES Hospitalizacia
);

ALTER TABLE Naordinovany_Liek ADD (
    CONSTRAINT FK_naordinovany_liek_liek
        FOREIGN KEY (id_liek) 
        REFERENCES Liek,
    CONSTRAINT FK_naordinovany_liek_hosp
        FOREIGN KEY (id_hospitalizacia) 
        REFERENCES Hospitalizacia
);


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Vytvorenie sekvencii

CREATE SEQUENCE seq_id_vysetrenie START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_lekar START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_sestra START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_liek START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_hospitalizacia START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_naordinovany_liek START WITH 1 INCREMENT BY 1;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Naplnenie tabuliek

INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Ferdič',
    'Martin',
    TO_DATE('16.11.1983', 'dd.mm.yyyy'),
    'Družby',
    54,
    'Piešťany',
    '92101',
    '0915775746',
    'MUDr.',
    'Chirurg, Ortopéd'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Morož',
    'Peter',
    TO_DATE('1.7.1987', 'dd.mm.yyyy'),
    'Palárikova',
    33,
    'Bratislava',
    '81105',
    '0905232432',
    'MUDr.',
    'Anesteziológ'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Turan',
    'Jozef',
    TO_DATE('24.3.1972', 'dd.mm.yyyy'),
    'Holekova',
    12,
    'Bratislava',
    '81104',
    '0915984442',
    'MUDr.',
    'Všeobecný lekár'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Lászlo',
    'Viktor',
    TO_DATE('20.12.1981', 'dd.mm.yyyy'),
    'Kvetná',
    '87',
    'Dunajská Streda',
    '92901',
    '0911923882',
    'MUDr.',
    'Ortopéd'
);

INSERT INTO Oddelenie VALUES (
    'CHI',
    'Chirurgia',
    1,
    '101'
);
INSERT INTO Oddelenie VALUES (
    'ORT',
    'Ortopédia',
    0,
    '001'
);
INSERT INTO Oddelenie VALUES (
    'INT',
    'Interné',
    0,
    '002'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Skačanová',
    'Mária',
    TO_DATE('4.8.1990', 'dd.mm.yyyy'),
    'Holekova',
    '36',
    'Bratislava',
    '81104',
    '0915112434',
    TO_DATE('2.2.2015', 'dd.mm.yyyy'),
    'CHI'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Kolesníkova',
    'Alžbeta',
    TO_DATE('13.3.1985', 'dd.mm.yyyy'),
    'Teplická',
    '36',
    'Piešťany',
    '92101',
    '0912124234',
    TO_DATE('2.2.2015', 'dd.mm.yyyy'),
    'CHI'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Zvončeková',
    'Katarína',
    TO_DATE('23.11.1983', 'dd.mm.yyyy'),
    'Horná',
    '22',
    'Dunajská Streda',
    '92901',
    '0915112224',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Brašnová',
    'Zuzana',
    TO_DATE('21.1.1979', 'dd.mm.yyyy'),
    'Bernolákova',
    '11',
    'Nové Mesto nad Váhom',
    '91501',
    '0905121114',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Javorinská',
    'Katarína',
    TO_DATE('1.8.1980', 'dd.mm.yyyy'),
    'Lesnícka',
    '54',
    'Nové Mesto nad Váhom',
    '91501',
    '0902772114',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Holubcová',
    'Petra',
    TO_DATE('22.5.1986', 'dd.mm.yyyy'),
    'Hlboká',
    '43',
    'Piešťany',
    '91501',
    '0903767112',
    TO_DATE('10.2.2015', 'dd.mm.yyyy'),
    'INT'
);

INSERT INTO Uvazok VALUES (
    'PRI',
    'Primár'
);
INSERT INTO Uvazok VALUES (
    'OSL',
    'Ošetrujúci lekár'
);
INSERT INTO Uvazok VALUES (
    'PRA',
    'Praktikant'
);

INSERT INTO Detail_o_sluzbe VALUES (
    1,
    'CHI',
    'PRI',
    TO_DATE('1.2.2015', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    2,
    'CHI',
    'OSL',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    TO_DATE('30.3.2015', 'dd.mm.yyyy')
);
INSERT INTO Detail_o_sluzbe VALUES (
    2,
    'INT',
    'OSL',
    TO_DATE('1.4.2015', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    3,
    'INT',
    'PRI',
    TO_DATE('1.12.2014', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    4,
    'ORT',
    'PRI',
    TO_DATE('1.2.2015', 'dd.mm.yyyy'),
    NULL
);

INSERT INTO Pacient VALUES (
    '8158086981',
    'Prasiak',
    'Gustáv',
    TO_DATE('13.5.1976', 'dd.mm.yyyy'),
    'Union',
    'Hlboká',
    30,
    'Piešťany',
    '92101',
    '0947148552'
);
INSERT INTO Pacient VALUES (
    '9311261487',
    'Kreken',
    'Dušan',
    TO_DATE('1.7.1950', 'dd.mm.yyyy'),
    'Dôvera',
    'A.Trajana',
    21,
    'Piešťany',
    '92101',
    '0912228597'
);
INSERT INTO Pacient VALUES (
    '8304224511',
    'Buxanto',
    'František',
    TO_DATE('22.4.1983', 'dd.mm.yyyy'),
    'VŠZP',
    'Brezová',
    45,
    'Piešťany',
    '92101',
    '0989711298'
);
INSERT INTO Pacient VALUES (
    '9002077711',
    'Goga',
    'Jozef',
    TO_DATE('7.2.1990', 'dd.mm.yyyy'),
    'VŠZP',
    'Dlhá',
    8,
    'Piešťany',
    '92101',
    '0905784217'
);
INSERT INTO Pacient VALUES (
    '7706240641',
    'Dubnička',
    'Ján',
    TO_DATE('15.2.1988', 'dd.mm.yyyy'),
    'VŠZP',
    'Pod Párovcami',
    122,
    'Piešťany',
    '92101',
    '0911268447'
);

INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '9311261487',
    3,
    TO_DATE('22.3.2015', 'dd.mm.yyyy'),
    NULL,
    'INT',
    010,
    'zápal mozgových blán',
    'samostatná izba'
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '8304224511',
    3,
    TO_DATE('25.2.2015', 'dd.mm.yyyy'),
    TO_DATE('27.2.2015', 'dd.mm.yyyy'),
    'INT',
    010,
    'otrava alkoholom',
    NULL
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '9002077711',
    1,
    TO_DATE('15.3.2015', 'dd.mm.yyyy'),
    NULL,
    'CHI',
    014,
    'operácia bedrového kĺbu',
    NULL
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '7706240641',
    4,
    TO_DATE('19.3.2015', 'dd.mm.yyyy'),
    NULL,
    'ORT',
    014,
    'zlomenina holennej kosti',
    NULL
);

INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '7706240641',
    4,
    TO_DATE('14.3.2015', 'dd.mm.yyyy'),
    NULL,
    'ORT',
    014,
    'zlomenina stehennej kosti',
    NULL
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('16.3.2015', 'dd.mm.yyyy'),
    4,
    2,
    'CHI',
    'Vykonaný röntgen holennej kosti u pacienta'
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('16.3.2015', 'dd.mm.yyyy'),
    3,
    2,
    'CHI',
    'Vykonaný röntgen stehennej kosti u pacienta'
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('16.3.2015', 'dd.mm.yyyy'),
    1,
    3,
    'CHI',
    'Vykonaný röntgen plúc u pacienta'
);

INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Paralen',
    'tableta',
    'paracetamol',
    'Kukuričný škrob predželatínovaný,povidón 30,sodná soľ kroskarmelózy,kyselina stearová',
    'Horúčka,bolesti zubov,hlavy,neuralgie, bolesti svalov alebo kĺbov.',
    'Precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok,ťažká hemolytická anémia,ťažké formy hepatálnej insuficiencie,akútna hepatitída.',
    'Podľa potreby v časovom odstupe najmenej 4 hodiny do maximálnej dennej dávky 8 tabliet'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Famciclovir - Teva',
    'Filmom obalená tableta',
    'famciklovir',
    'Mikrokryštalická celulóza E460,Koloidný oxid kremičitý E551,Sodná soľ karboxymetylškrobu',
    'Liečba infekcií pohlavných orgánov herpetickým vírusom,Liečba infekcií kože a slizníc vyvolaných vírusom Herpes zoster.',
    'Precitlivenosť na famciklovir alebo na niektorú z pomocných látok. Precitlivenosť na penciklovir.',
    '250 mg trikrát denne počas 5 dní'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Nalgesin FORTE 550mg',
    'Filmom obalená tableta',
    'sodná soľ naproxénu',
    'Mikrokryštalická celulóza E460,mastenec (E553b),magnéziumstearát (E572)',
    'Tlmí bolesť a zápal a znižuje horúčku,bolesť zubov a hlavy,bolesť v svaloch, kĺboch a chrbtici,prevenciu a liečbu migrény.',
    'Precitlivenosť na salyciláty a iné nesteroidné protizápalové lieky určené na liečbu.',
    '275 mg každých 6 až 8 hodín'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Paclitaxel Kabi 6 mg/ml ',
    'infúzny koncentrát',
    'paklitaxel',
    'Etanol(bezvodý),Ricínoleoylmakrogol-glycerol,Kyselina citrónová(bezvodá)',
    'Liečba pacientok s pokročilým ovariálnym karcinómom alebo s reziduálnou chorobou.',
    'Paklitaxel sa nemá používať u pacientov s východiskovým počtom neutrofilov.',
    '20 mg perorálne približne 12 a 6 hodín'
);

INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    1,
    1,
    TO_DATE('22.3.2015', 'dd.mm.yyyy'),
    '3x denne',
    '1 tableta ',
    NULL
);
INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    3,
    1,
    TO_DATE('20.3.2015', 'dd.mm.yyyy'),
    'každých 6 hodín',
    '1 tableta',
    'tabletu užívať po jedle'
);
INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    3,
    3,
    TO_DATE('15.3.2015', 'dd.mm.yyyy'),
    'každých 6 hodín',
    'polovica tablety',
    NULL
);


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Vypisy 

--SELECT * FROM Lekar;
--SELECT * FROM Sestra;
--SELECT * FROM Oddelenie;
--SELECT * FROM Uvazok;
--SELECT * FROM Detail_o_sluzbe;
--SELECT * FROM Vysetrenie;
--SELECT * FROM Pacient;
--SELECT * FROM Liek;
--SELECT * FROM Hospitalizacia;
--SELECT * FROM Naordinovany_Liek;


-- Zobrazi informacie o liekoch (nazov lieku, forma lieku, zaciatok_uzivania, davkovanie, mnozstvo) ktore
-- boli podane pacientovi "Dusanovi Krekenovi" ked bol hospitalizovany dna 22.3.2015.
SELECT L.nazov, L.forma, NL.zaciatok_uzivania, NL.davkovanie, NL.mnozstvo
FROM Pacient P, Hospitalizacia H, Liek L, Naordinovany_Liek NL
WHERE P.rc = H.rc_pacient AND H.id = NL.id_hospitalizacia AND NL.id_liek = L.id
    AND P.meno = 'Dušan' AND P.priezvisko = 'Kreken' 
    AND H.zaciatok = TO_DATE('22.3.2015', 'dd.mm.yyyy');

-- Zobrazi skratku oddelenia, nazov oddelenia a pocet sestier na danom oddeleni zoradene vzostupne
SELECT O.skratka, O.nazov, COUNT(S.id_oddelenie) pocet_sestier
FROM Oddelenie O INNER JOIN Sestra S ON O.skratka = S.id_oddelenie
GROUP BY O.skratka, O.nazov
ORDER BY pocet_sestier ASC;

-- Zobrazi informacie (id, titul, priezvisko, meno) o lekaroch ktori hospitalizovali viac ako 1 pacienta
SELECT L.id, L.titul, L.priezvisko, L.meno, COUNT(H.rc_pacient) pocet_hospitalizacii
FROM Lekar L INNER JOIN Hospitalizacia H ON L.id = H.id_lekar
GROUP BY L.id, L.titul ,L.priezvisko, L.meno
HAVING COUNT(H.rc_pacient) > 1;

-- Zobrazi informacie (id, titul, priezvisko, meno, nazov oddelenia, nazov uvazku, zaciatok sluzby a koniec sluzby)
-- o lekaroch a ich uvazkoch na oddeleniach
SELECT L.id, L.titul, L.priezvisko, L.meno, O.nazov, U.nazov, D.zaciatok_sluzby, D.koniec_sluzby
FROM Lekar L, Oddelenie O, Detail_o_sluzbe D, Uvazok U
WHERE L.id = D.id_lekar AND D.skratka_oddelenie = O.skratka
    AND D.skratka_uvazok = U.skratka;

-- Zobrazi vsetkych pacientov ktori boli hospitalizovani
-- medzi 16.3.2015 a 24.3.2015
SELECT P.rc, P.priezvisko, P.meno, H.zaciatok, O.Nazov, H.cislo_izby
FROM Pacient P INNER JOIN Hospitalizacia H ON P.rc = H.rc_pacient 
    INNER JOIN Oddelenie O ON H.id_oddelenie = O.skratka
WHERE H.zaciatok BETWEEN TO_DATE('16.3.2015', 'dd.mm.yyyy') AND TO_DATE('24.3.2015', 'dd.mm.yyyy');

-- Zobrazi pacientov ktori nepodstupili ziadne vysetrenie
SELECT DISTINCT P.*
FROM Pacient P, Hospitalizacia H
WHERE (P.rc = H.rc_pacient AND NOT EXISTS (SELECT * FROM Vysetrenie V WHERE H.id = V.id_hospitalizacia))
    OR
    P.rc NOT IN (SELECT DISTINCT H.rc_pacient FROM Hospitalizacia H);

-- Zobrazi lekarov ktori vykonali nejake vysetrenie
SELECT L.*
FROM Lekar L
WHERE L.id IN (SELECT DISTINCT V.id_lekar FROM Vysetrenie V);

-- Zobrazi udaje o pacientoch a liekoch, ktore boli naordinovane pri hospitalizaciach
-- medzi 20.3.2015 - 27.3.2015
SELECT NL.id, NL.zaciatok_uzivania, L.id, L.nazov, H.id, H.rc_pacient,P.meno, P.priezvisko
FROM Naordinovany_Liek NL, Liek L, Hospitalizacia H, Pacient P
WHERE (H.zaciatok BETWEEN TO_DATE('15.3.2015', 'dd.mm.yyyy') AND TO_DATE('22.3.2015', 'dd.mm.yyyy'))
    AND NL.id_hospitalizacia = H.id AND NL.id = L.id AND P.rc = H.rc_pacient;

-- Zobrazi udaje o pacientoch, ktori boli hospitalizovani viac ako jeden krat
SELECT P.meno, P.priezvisko, P.rc, P.zdrav_poistovna, COUNT(H.rc_pacient) AS pocet_hospitalizacii
FROM Pacient P INNER JOIN Hospitalizacia H ON P.rc = H.rc_pacient
GROUP BY P.meno, P.priezvisko, P.rc, P.zdrav_poistovna
HAVING COUNT(H.rc_pacient) > 1;
