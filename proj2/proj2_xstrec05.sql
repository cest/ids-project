
DROP TABLE Hospitalizacia CASCADE CONSTRAINTS;
DROP TABLE Naordinovany_Liek CASCADE CONSTRAINTS;
DROP TABLE Pacient CASCADE CONSTRAINTS;
DROP TABLE Liek CASCADE CONSTRAINTS;
DROP SEQUENCE seq_ic_hospitalizacie;
DROP SEQUENCE seq_ic_lieku;
DROP SEQUENCE seq_ic_naordinovaneho_lieku;


CREATE TABLE Hospitalizacia
(
	ic	INTEGER,
	zaciatok DATE NOT NULL,
	koniec DATE,
	cislo_izby INTEGER,
	diagnoza VARCHAR(255),
	specialne_poziadavky VARCHAR(255)
);

CREATE TABLE Pacient
(
	rodne_cislo	VARCHAR(11), --osetrit aby bol spravny tvar
	priezvisko VARCHAR(30) NOT NULL,
	meno VARCHAR(30) NOT NULL,
	ulica VARCHAR(20),
	cislo CHAR(5),
	mesto VARCHAR(20),
	psc CHAR(5),
	datum_narodenia DATE,
	zdravotna_poistovna VARCHAR(30),
	tel_cislo CHAR(10)
);

CREATE TABLE Naordinovany_Liek
(
	ic INTEGER,
	nazov VARCHAR(30) NOT NULL,
	zaciatok_uzivania DATE NOT NULL,
	davkovanie VARCHAR(50),
	mnozstvo VARCHAR(50),
	specifikacie VARCHAR(255)
);

CREATE TABLE Liek
(
	ic INTEGER,
	nazov VARCHAR(30) NOT NULL,
	ucinna_latka VARCHAR(50) NOT NULL,
	zlozenie VARCHAR(255) NOT NULL,
	indikacie VARCHAR(255) NOT NULL,
	kontraindikacie VARCHAR(255),
	forma VARCHAR(255),
	odporucane_davkovanie VARCHAR(255)
);


ALTER TABLE Hospitalizacia ADD CONSTRAINT PK_hospitalizacia PRIMARY KEY (ic);
ALTER TABLE Pacient ADD CONSTRAINT PK_pacient PRIMARY KEY (rodne_cislo);
ALTER TABLE Naordinovany_Liek ADD CONSTRAINT PK_naordinovany_liek PRIMARY KEY (ic);
ALTER TABLE Liek ADD CONSTRAINT PK_liek PRIMARY KEY (ic);

CREATE SEQUENCE seq_ic_lieku START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE seq_ic_hospitalizacie START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE seq_ic_naordinovaneho_lieku START WITH 1 INCREMENT BY 1;

INSERT INTO Pacient VALUES(seq_ic_lekara.NEXTVAL,'Prasiak','Gustáv','Hlboká','30','Piešťany','92101',TO_DATE('13.5.1976', 'dd.mm.yyyy'),'Union','0947148552');
INSERT INTO Pacient VALUES(seq_ic_lekara.NEXTVAL,'Kreken','Dušan','A.Trajana','21','Piešťany','92101',TO_DATE('1.7.1950', 'dd.mm.yyyy'),'Dovera','0912228597');
INSERT INTO Pacient VALUES(seq_ic_lekara.NEXTVAL,'Buxanto','František','Brezová','45','Piešťany','92101',TO_DATE('22.4.1983', 'dd.mm.yyyy'),'VSZP','0989711298');
INSERT INTO Pacient VALUES(seq_ic_lekara.NEXTVAL,'Goga','Jozef','Dlhá','8','Piešťany','92101',TO_DATE('7.2.1990', 'dd.mm.yyyy'),'VSZP','0905784217');
INSERT INTO Pacient VALUES(seq_ic_lekara.NEXTVAL,'Dubnička','Ján','Pod Párovcami','122','Piešťany','92101',TO_DATE('15.2.1988', 'dd.mm.yyyy'),'VSZP','0911268447');

INSERT INTO Liek VALUES(seq_ic_lieku.NEXTVAL,'Paralen','paracetamol',
	'kukuričný škrob predželatínovaný,povidón 30,sodná soľ kroskarmelózy,kyselina stearová','Horúčka,bolesti zubov,hlavy,neuralgie, bolesti svalov alebo kĺbov',
	'precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok,ťažká hemolytická anémia,ťažké formy hepatálnej insuficiencie,akútna hepatitída','tableta',
	'podľa potreby v časovom odstupe najmenej 4 hodiny do maximálnej dennej dávky 8 tabliet');

INSERT INTO Liek VALUES(seq_ic_lieku.NEXTVAL,'Famciclovir - Teva','famciklovir',
	'Mikrokryštalická celulóza E460,Koloidný oxid kremičitý E551,Sodná soľ karboxymetylškrobu','Liečba infekcií pohlavných orgánov herpetickým vírusom,Liečba infekcií kože a slizníc vyvolaných vírusom Herpes zoster',
	'Precitlivenosť na famciklovir alebo na niektorú z pomocných látok. Precitlivenosť na penciklovir.','Filmom obalená tableta',
	'250 mg trikrát denne počas 5 dní');

INSERT INTO Liek VALUES(seq_ic_lieku.NEXTVAL,'Nalgesin FORTE 550mg','sodná soľ naproxénu',
	'Mikrokryštalická celulóza E460,mastenec (E553b),magnéziumstearát (E572)','tlmí bolesť a zápal a znižuje horúčku,bolesť zubov a hlavy,bolesť v svaloch, kĺboch a chrbtici,prevenciu a liečbu migrény,',
	'Precitlivenosť na salyciláty a iné nesteroidné protizápalové lieky určené na liečbu','Filmom obalená tableta',
	'275 mg každých 6 až 8 hodín');

INSERT INTO Liek VALUES(seq_ic_lieku.NEXTVAL,'Paclitaxel Kabi 6 mg/ml ','paklitaxel',
	'Etanol(bezvodý),Ricínoleoylmakrogol-glycerol,Kyselina citrónová(bezvodá)','liečba pacientok s pokročilým ovariálnym karcinómom alebo s reziduálnou chorobou',
	'Paklitaxel sa nemá používať u pacientov s východiskovým počtom neutrofilov','Infúzny koncentrát',
	'20 mg perorálne približne 12 a 6 hodín');

INSERT INTO Naordinovany_Liek VALUES(seq_ic_naordinovaneho_lieku.NEXTVAL,'Paralen',TO_DATE('24.3.2015', 'dd.mm.yyyy'),'1 tableta 3x denne','20 tabliet',NULL);
INSERT INTO Naordinovany_Liek VALUES(seq_ic_naordinovaneho_lieku.NEXTVAL,'Famciclovir - Teva',TO_DATE('20.3.2015', 'dd.mm.yyyy'),'polovica tablety, 3x denne','15 tabliet',NULL);
INSERT INTO Naordinovany_Liek VALUES(seq_ic_naordinovaneho_lieku.NEXTVAL,'Nalgesin FORTE 550mg',TO_DATE('15.1.2015', 'dd.mm.yyyy'),'polovica tablety každých 6 hodín','20 tabliet',NULL);
INSERT INTO Naordinovany_Liek VALUES(seq_ic_naordinovaneho_lieku.NEXTVAL,'Paralen',TO_DATE('10.3.2015', 'dd.mm.yyyy'),'1 tableta 3x denne','20 tabliet',NULL);

INSERT INTO Hospitalizacia VALUES(seq_ic_hospitalizacie.NEXTVAL,TO_DATE('22.3.2015', 'dd.mm.yyyy'),NULL,110,'angina pectoris','samostatná izba');
INSERT INTO Hospitalizacia VALUES(seq_ic_hospitalizacie.NEXTVAL,TO_DATE('25.2.2015', 'dd.mm.yyyy'),TO_DATE('27.2.2015', 'dd.mm.yyyy'),110,'otrava alkoholom',NULL);
INSERT INTO Hospitalizacia VALUES(seq_ic_hospitalizacie.NEXTVAL,TO_DATE('15.2.2015', 'dd.mm.yyyy'),NULL,114,'operácia bedrového kĺbu',NULL);
INSERT INTO Hospitalizacia VALUES(seq_ic_hospitalizacie.NEXTVAL,TO_DATE('19.3.2015', 'dd.mm.yyyy'),NULL,114,'zápal slepého čreva',NULL);
INSERT INTO Hospitalizacia VALUES(seq_ic_hospitalizacie.NEXTVAL,TO_DATE('24.3.2015', 'dd.mm.yyyy'),NULL,115,'infarkt',NULL);

--SELECT * FROM Pacient;
--SELECT * FROM Liek;
--SELECT * FROM Hospitalizacia;
SELECT * FROM Naordinovany_Liek;

