
DROP TABLE Lekar CASCADE CONSTRAINTS;
DROP TABLE Vysetrenie CASCADE CONSTRAINTS;
DROP TABLE Sestra CASCADE CONSTRAINTS;
DROP TABLE Oddelenie CASCADE CONSTRAINTS;
DROP TABLE Detaily_o_sluzbach CASCADE CONSTRAINTS;
DROP TABLE Uvazok CASCADE CONSTRAINTS;
DROP SEQUENCE seq_ic_vysetrenia;
DROP SEQUENCE seq_ic_lekara;
DROP SEQUENCE seq_ic_sestry;

CREATE TABLE Lekar (
ic  INTEGER,
priezvisko VARCHAR(20) NOT NULL,
meno VARCHAR(20) NOT NULL,
ulica VARCHAR(20),
cislo VARCHAR(5),
mesto VARCHAR(20),
psc CHAR(5),
datum_narodenia DATE,
telefon CHAR(10),
titul VARCHAR(10),
kvalifikacia VARCHAR(255)
);

CREATE TABLE Vysetrenie (
ic  INTEGER,
nazov VARCHAR(50) NOT NULL,
datum DATE,
oddelenie CHAR(3),
--hospitalizacia INTEGER,
lekar INTEGER,
detaily VARCHAR(255)
);

CREATE TABLE Sestra (
ic  INTEGER,
priezvisko VARCHAR(20) NOT NULL,
meno VARCHAR(20) NOT NULL,
ulica VARCHAR(20),
cislo CHAR(5),
mesto VARCHAR(20),
psc CHAR(5),
datum_narodenia DATE,
telefon CHAR(10),
datum_preskolenia DATE,
oddelenie CHAR(3)
);

CREATE TABLE Oddelenie (
skratka  CHAR(3),
nazov VARCHAR(20) NOT NULL,
poschodie INTEGER,
telefon CHAR(3)
);

CREATE TABLE Detaily_o_sluzbach (
lekar  INTEGER,
oddelenie CHAR(3),
zaciatok_sluzby DATE,
koniec_sluzby DATE,
uvazok CHAR(3)
);

CREATE TABLE Uvazok (
skratka  CHAR(3),
oddelenie VARCHAR(20)
);  

ALTER TABLE Lekar ADD CONSTRAINT PK_lekar PRIMARY KEY (ic);
ALTER TABLE Vysetrenie ADD CONSTRAINT PK_vysetrenie PRIMARY KEY (ic);
ALTER TABLE Sestra ADD CONSTRAINT PK_sestra PRIMARY KEY (ic);
ALTER TABLE Oddelenie ADD CONSTRAINT PK_oddelenie PRIMARY KEY (skratka);
ALTER TABLE Uvazok ADD CONSTRAINT PK_uvazok PRIMARY KEY (skratka);
ALTER TABLE Detaily_o_sluzbach ADD CONSTRAINT PK_detaily PRIMARY KEY (lekar, oddelenie);
ALTER TABLE Vysetrenie ADD CONSTRAINT FK_vysetrenie_oddelenie FOREIGN KEY (oddelenie)
   REFERENCES Oddelenie ON DELETE CASCADE;
ALTER TABLE Vysetrenie ADD CONSTRAINT FK_vysetrenie_lekar FOREIGN KEY (lekar) 
  REFERENCES Lekar ON DELETE CASCADE;
ALTER TABLE Sestra ADD CONSTRAINT FK_sestra_oddelenie FOREIGN KEY (oddelenie) 
   REFERENCES Oddelenie ON DELETE CASCADE;
ALTER TABLE Detaily_o_sluzbach ADD CONSTRAINT FK_detaily_lekar FOREIGN KEY (lekar) 
   REFERENCES Lekar ON DELETE CASCADE;
ALTER TABLE Detaily_o_sluzbach ADD CONSTRAINT FK_detaily_oddelenie FOREIGN KEY (oddelenie) 
   REFERENCES Oddelenie ON DELETE CASCADE;
ALTER TABLE Detaily_o_sluzbach ADD CONSTRAINT FK_detaily_uvazok FOREIGN KEY (uvazok) 
   REFERENCES Uvazok ON DELETE CASCADE;

CREATE SEQUENCE seq_ic_vysetrenia START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE seq_ic_lekara START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE seq_ic_sestry START WITH 1 INCREMENT BY 1;

INSERT INTO Lekar VALUES(seq_ic_lekara.NEXTVAL,'Ferdič','Martin','Družby','54','Piešťany','92101',TO_DATE('16.11.1983', 'dd.mm.yyyy'),'0915775746','MUDr.','Chirurg');
INSERT INTO Lekar VALUES(seq_ic_lekara.NEXTVAL,'Morož','Peter','Palárikova','33','Bratislava','81105',TO_DATE('1.7.1987', 'dd.mm.yyyy'),'0905232432','MUDr.','Anesteziológ');
INSERT INTO Lekar VALUES(seq_ic_lekara.NEXTVAL,'Turan','Jozef','Holekova','12','Bratislava','81104',TO_DATE('24.3.1972', 'dd.mm.yyyy'),'0915984442','MUDr.','Gynekológ');
INSERT INTO Lekar VALUES(seq_ic_lekara.NEXTVAL,'Lászlo','Viktor','Kvetná','87','Dunajská Streda','92901',TO_DATE('20.12.1981', 'dd.mm.yyyy'),'0911923882','MUDr.','Ortopéd');

INSERT INTO Oddelenie VALUES('CHI','Chirurgia',1,'101');
INSERT INTO Oddelenie VALUES('ORT','Ortopédia',0,'001');
INSERT INTO Oddelenie VALUES('GYN','Gynekológia',2,'201');

INSERT INTO Uvazok VALUES('PRI','Primár');
INSERT INTO Uvazok VALUES('OSL','Ošetrujúci lekár');
INSERT INTO Uvazok VALUES('PRA','Praktikant');

INSERT INTO Sestra VALUES(seq_ic_sestry.NEXTVAL,'Skačanová','Mária','Holekova','36','Bratislava','81104',TO_DATE('4.8.1990', 'dd.mm.yyyy'),'0915112434',TO_DATE('2.2.2015', 'dd.mm.yyyy'),'GYN');

INSERT INTO Vysetrenie VALUES(seq_ic_vysetrenia.NEXTVAL,'Sono',TO_DATE('16.3.2015', 'dd.mm.yyyy'),'GYN',3,'Vykonané sono u pacienta');

INSERT INTO Detaily_o_sluzbach VALUES(3,'GYN',TO_DATE('16.3.2015', 'dd.mm.yyyy'),TO_DATE('18.3.2015', 'dd.mm.yyyy'),'PRI');

--SELECT * FROM Lekar;
--SELECT * FROM Sestra;
--SELECT * FROM Oddelenie;
--SELECT * FROM Uvazok;
--SELECT * FROM Vysetrenie;
--SELECT * FROM Detaily_o_sluzbach
