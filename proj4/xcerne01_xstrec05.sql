-- IDS PROJECT 4
-- 1.5.2015
-- xcerne01@stud.fit.vutbr.cz
-- xstrec05@stud.fit.vutbr.cz


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Zrusenie tabuliek a sekvencii

DROP TABLE Lekar CASCADE CONSTRAINTS;
DROP TABLE Vysetrenie CASCADE CONSTRAINTS;
DROP TABLE Sestra CASCADE CONSTRAINTS;
DROP TABLE Oddelenie CASCADE CONSTRAINTS;
DROP TABLE Detail_o_sluzbe CASCADE CONSTRAINTS;
DROP TABLE Uvazok CASCADE CONSTRAINTS;
DROP TABLE Hospitalizacia CASCADE CONSTRAINTS;
DROP TABLE Naordinovany_Liek CASCADE CONSTRAINTS;
DROP TABLE Liek CASCADE CONSTRAINTS;
DROP TABLE Pacient CASCADE CONSTRAINTS;
DROP SEQUENCE seq_id_vysetrenie;
DROP SEQUENCE seq_id_lekar;
DROP SEQUENCE seq_id_sestra;
DROP SEQUENCE seq_id_hospitalizacia;
DROP SEQUENCE seq_id_liek;
DROP SEQUENCE seq_id_naordinovany_liek;
DROP PROCEDURE aktualizuj_datum_preskolenia;
DROP PROCEDURE aktualne_hospitalizovany;

DROP MATERIALIZED VIEW Lekar_view;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Vytvorenie tabuliek

CREATE TABLE Lekar (
    id                  INTEGER         PRIMARY KEY,
    priezvisko          VARCHAR2(20)    NOT NULL,
    meno                VARCHAR2(20)    NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    ulica               VARCHAR2(20)    NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               VARCHAR2(40)    NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE,
    titul               VARCHAR2(10),
    kvalifikacia        VARCHAR2(100)
);

CREATE TABLE Oddelenie (
    skratka             CHAR(3)         PRIMARY KEY,
    nazov               VARCHAR2(20)    NOT NULL,
    poschodie           INTEGER,
    telefon             CHAR(3)         UNIQUE   -- Rychla volba, preto len 3 znaky
);

CREATE TABLE Sestra (
    id                  INTEGER         PRIMARY KEY,
    priezvisko          NVARCHAR2(20)   NOT NULL,
    meno                NVARCHAR2(20)   NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    ulica               NVARCHAR2(20)   NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               NVARCHAR2(40)   NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE,
    datum_preskolenia   DATE,
    id_oddelenie        CHAR(3)         NOT NULL
);

CREATE TABLE Uvazok (
    skratka             CHAR(3)         PRIMARY KEY,
    nazov               NVARCHAR2(20)   NOT NULL
);

CREATE TABLE Detail_o_sluzbe (
    id_lekar            INTEGER         NOT NULL,
    skratka_oddelenie   CHAR(3)         NOT NULL,
    skratka_uvazok      CHAR(3)         NOT NULL,
    zaciatok_sluzby     DATE            NOT NULL,
    koniec_sluzby       DATE
);

CREATE TABLE Pacient (
    rc                  CHAR(10)        PRIMARY KEY,
    priezvisko          NVARCHAR2(30)   NOT NULL,
    meno                NVARCHAR2(30)   NOT NULL,
    datum_narodenia     DATE            NOT NULL,
    zdrav_poistovna     NVARCHAR2(30)   NOT NULL,
    ulica               NVARCHAR2(20)   NOT NULL,
    cislo               INTEGER         NOT NULL,
    mesto               NVARCHAR2(40)   NOT NULL,
    psc                 CHAR(5)         NOT NULL,
    telefon             CHAR(10)        UNIQUE
);

CREATE TABLE Hospitalizacia (
    id                  INTEGER         PRIMARY KEY,
    rc_pacient          CHAR(10)        NOT NULL,
    id_lekar            INTEGER         NOT NULL,
    zaciatok            DATE            NOT NULL,
    koniec              DATE,
    id_oddelenie        CHAR(3)         NOT NULL,
    cislo_izby          INTEGER         NOT NULL,
    diagnoza            NVARCHAR2(100),
    spec_poziadavky     NVARCHAR2(100)
);

CREATE TABLE Vysetrenie (
    id                  INTEGER         PRIMARY KEY,
    nazov               NVARCHAR2(50)   NOT NULL,
    datum               DATE            NOT NULL,
    id_hospitalizacia   INTEGER         NOT NULL,
    id_lekar            INTEGER         NOT NULL,
    skratka_oddelenie   CHAR(3)         NOT NULL,
    detaily             CLOB
);

CREATE TABLE Liek (
    id                  INTEGER         PRIMARY KEY,
    nazov               NVARCHAR2(30)   NOT NULL,
    forma               NVARCHAR2(50),
    ucinna_latka        NVARCHAR2(50),
    zlozenie            NVARCHAR2(255),
    indikacie           NVARCHAR2(255),
    kontraindikacie     NVARCHAR2(255),
    odporuc_davkovanie  NVARCHAR2(100)
);

CREATE TABLE Naordinovany_Liek (
    id                  INTEGER         PRIMARY KEY,
    id_liek             INTEGER         NOT NULL,
    id_hospitalizacia   INTEGER         NOT NULL,
    zaciatok_uzivania   DATE            NOT NULL,
    davkovanie          NVARCHAR2(50),
    mnozstvo            NVARCHAR2(50),
    specifikacie        NVARCHAR2(255)
);

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Pridanie integritnych omedzeni a checkov

ALTER TABLE Lekar ADD (
    CONSTRAINT CH_lekar_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_lekar_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_lekar_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Oddelenie ADD (
    CONSTRAINT CH_oddelenie_poschodie
        CHECK (poschodie >= 0),
    CONSTRAINT CH_oddelenie_telefon
        CHECK (REGEXP_LIKE(telefon,'[0-9]{3}'))
);

ALTER TABLE Sestra ADD (
    CONSTRAINT FK_sestra_oddelenie
        FOREIGN KEY (id_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT CH_sestra_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_sestra_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_sestra_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Detail_o_sluzbe ADD (
    CONSTRAINT PK_detaily_lekar_oddelenie 
        PRIMARY KEY (id_lekar, skratka_oddelenie),
    CONSTRAINT FK_detaily_lekar 
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT FK_detaily_oddelenie 
        FOREIGN KEY (skratka_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT FK_detaily_uvazok
        FOREIGN KEY (skratka_uvazok) 
        REFERENCES Uvazok
);

ALTER TABLE Pacient ADD (
    CONSTRAINT CH_pacient_rc
        CHECK (REGEXP_LIKE(rc,'[0-9]{10}')),
    CONSTRAINT CH_pacient_zdrav_poistovna
        CHECK (zdrav_poistovna in ('Dôvera','Union','VŠZP')),
    CONSTRAINT CH_pacient_cislo
        CHECK (cislo > 0),
    CONSTRAINT CH_pacient_psc
        CHECK (REGEXP_LIKE(psc,'[0-9]{5}')),
    CONSTRAINT CH_pacient_telefon
        CHECK (REGEXP_LIKE(telefon,'09[0-9]{8}'))
);

ALTER TABLE Hospitalizacia ADD (
    CONSTRAINT FK_hospitalizacia_pacient_rc 
        FOREIGN KEY (rc_pacient) 
        REFERENCES Pacient,
    CONSTRAINT FK_hospitalizacia_oddelenie 
        FOREIGN KEY (id_oddelenie) 
        REFERENCES Oddelenie,
    CONSTRAINT FK_hospitalizacia_lekar
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT CH_hospitalizacia_cislo_izby
        CHECK (cislo_izby > 0)
);

ALTER TABLE Vysetrenie ADD (
    CONSTRAINT FK_vysetrenie_oddelenie
        FOREIGN KEY (skratka_oddelenie)
        REFERENCES Oddelenie,
    CONSTRAINT FK_vysetrenie_lekar
        FOREIGN KEY (id_lekar) 
        REFERENCES Lekar,
    CONSTRAINT FK_vysetrenie_hospitalizacia
        FOREIGN KEY (id_hospitalizacia) 
        REFERENCES Hospitalizacia
);

ALTER TABLE Naordinovany_Liek ADD (
    CONSTRAINT FK_naordinovany_liek_liek
        FOREIGN KEY (id_liek) 
        REFERENCES Liek,
    CONSTRAINT FK_naordinovany_liek_hosp
        FOREIGN KEY (id_hospitalizacia) 
        REFERENCES Hospitalizacia
);


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Vytvorenie sekvencii

CREATE SEQUENCE seq_id_vysetrenie START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_lekar START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_sestra START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_liek START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_hospitalizacia START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE seq_id_naordinovany_liek START WITH 1 INCREMENT BY 1;

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Naplnenie tabuliek

INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Ferdič',
    'Martin',
    TO_DATE('16.11.1983', 'dd.mm.yyyy'),
    'Družby',
    54,
    'Piešťany',
    '92101',
    '0915775746',
    'MUDr.',
    'Chirurg, Ortopéd'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Morož',
    'Peter',
    TO_DATE('1.7.1987', 'dd.mm.yyyy'),
    'Palárikova',
    33,
    'Bratislava',
    '81105',
    '0905232432',
    'MUDr.',
    'Anesteziológ'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Turan',
    'Jozef',
    TO_DATE('24.3.1972', 'dd.mm.yyyy'),
    'Holekova',
    12,
    'Bratislava',
    '81104',
    '0915984442',
    'MUDr.',
    'Všeobecný lekár'
);
INSERT INTO Lekar VALUES (
    seq_id_lekar.NEXTVAL,
    'Lászlo',
    'Viktor',
    TO_DATE('20.12.1981', 'dd.mm.yyyy'),
    'Kvetná',
    '87',
    'Dunajská Streda',
    '92901',
    '0911923882',
    'MUDr.',
    'Ortopéd'
);

INSERT INTO Oddelenie VALUES (
    'CHI',
    'Chirurgia',
    1,
    '101'
);
INSERT INTO Oddelenie VALUES (
    'ORT',
    'Ortopédia',
    0,
    '001'
);
INSERT INTO Oddelenie VALUES (
    'INT',
    'Interné',
    0,
    '002'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Skačanová',
    'Mária',
    TO_DATE('4.8.1990', 'dd.mm.yyyy'),
    'Holekova',
    '36',
    'Bratislava',
    '81104',
    '0915112434',
    TO_DATE('2.2.2015', 'dd.mm.yyyy'),
    'CHI'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Kolesníkova',
    'Alžbeta',
    TO_DATE('13.3.1985', 'dd.mm.yyyy'),
    'Teplická',
    '36',
    'Piešťany',
    '92101',
    '0912124234',
    TO_DATE('2.2.2015', 'dd.mm.yyyy'),
    'CHI'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Zvončeková',
    'Katarína',
    TO_DATE('23.11.1983', 'dd.mm.yyyy'),
    'Horná',
    '22',
    'Dunajská Streda',
    '92901',
    '0915112224',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Brašnová',
    'Zuzana',
    TO_DATE('21.1.1979', 'dd.mm.yyyy'),
    'Bernolákova',
    '11',
    'Nové Mesto nad Váhom',
    '91501',
    '0905121114',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Javorinská',
    'Katarína',
    TO_DATE('1.8.1980', 'dd.mm.yyyy'),
    'Lesnícka',
    '54',
    'Nové Mesto nad Váhom',
    '91501',
    '0902772114',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    'ORT'
);

INSERT INTO Sestra VALUES (
    seq_id_sestra.NEXTVAL,
    'Holubcová',
    'Petra',
    TO_DATE('22.5.1986', 'dd.mm.yyyy'),
    'Hlboká',
    '43',
    'Piešťany',
    '91501',
    '0903767112',
    TO_DATE('10.2.2015', 'dd.mm.yyyy'),
    'INT'
);

INSERT INTO Uvazok VALUES (
    'PRI',
    'Primár'
);
INSERT INTO Uvazok VALUES (
    'OSL',
    'Ošetrujúci lekár'
);
INSERT INTO Uvazok VALUES (
    'PRA',
    'Praktikant'
);

INSERT INTO Detail_o_sluzbe VALUES (
    1,
    'CHI',
    'PRI',
    TO_DATE('1.2.2015', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    2,
    'CHI',
    'OSL',
    TO_DATE('1.3.2015', 'dd.mm.yyyy'),
    TO_DATE('30.3.2015', 'dd.mm.yyyy')
);
INSERT INTO Detail_o_sluzbe VALUES (
    2,
    'INT',
    'OSL',
    TO_DATE('1.4.2015', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    3,
    'INT',
    'PRI',
    TO_DATE('1.12.2014', 'dd.mm.yyyy'),
    NULL
);
INSERT INTO Detail_o_sluzbe VALUES (
    4,
    'ORT',
    'PRI',
    TO_DATE('1.2.2015', 'dd.mm.yyyy'),
    NULL
);

INSERT INTO Pacient VALUES (
    '8158086981',
    'Prasiak',
    'Gustáv',
    TO_DATE('13.5.1976', 'dd.mm.yyyy'),
    'Union',
    'Hlboká',
    30,
    'Piešťany',
    '92101',
    '0947148552'
);
INSERT INTO Pacient VALUES (
    '9311261487',
    'Kreken',
    'Dušan',
    TO_DATE('1.7.1950', 'dd.mm.yyyy'),
    'Dôvera',
    'A.Trajana',
    21,
    'Piešťany',
    '92101',
    '0912228597'
);
INSERT INTO Pacient VALUES (
    '8304224511',
    'Buxanto',
    'František',
    TO_DATE('22.4.1983', 'dd.mm.yyyy'),
    'VŠZP',
    'Brezová',
    45,
    'Piešťany',
    '92101',
    '0989711298'
);
INSERT INTO Pacient VALUES (
    '9002077711',
    'Goga',
    'Jozef',
    TO_DATE('7.2.1990', 'dd.mm.yyyy'),
    'VŠZP',
    'Dlhá',
    8,
    'Piešťany',
    '92101',
    '0905784217'
);
INSERT INTO Pacient VALUES (
    '7706240641',
    'Dubnička',
    'Ján',
    TO_DATE('15.2.1988', 'dd.mm.yyyy'),
    'VŠZP',
    'Pod Párovcami',
    122,
    'Piešťany',
    '92101',
    '0911268447'
);

INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '9311261487',
    3,
    TO_DATE('22.3.2015', 'dd.mm.yyyy'),
    NULL,
    'INT',
    010,
    'zápal mozgových blán',
    'samostatná izba'
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '8304224511',
    3,
    TO_DATE('25.2.2015', 'dd.mm.yyyy'),
    TO_DATE('27.2.2015', 'dd.mm.yyyy'),
    'INT',
    010,
    'otrava alkoholom',
    NULL
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '9002077711',
    1,
    TO_DATE('15.3.2015', 'dd.mm.yyyy'),
    NULL,
    'CHI',
    014,
    'operácia bedrového kĺbu',
    NULL
);
INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '7706240641',
    4,
    TO_DATE('19.3.2014', 'dd.mm.yyyy'),
    TO_DATE('24.4.2014', 'dd.mm.yyyy'),
    'ORT',
    014,
    'zlomenina holennej kosti',
    NULL
);

INSERT INTO Hospitalizacia VALUES (
    seq_id_hospitalizacia.NEXTVAL,
    '7706240641',
    4,
    TO_DATE('14.3.2015', 'dd.mm.yyyy'),
    NULL,
    'ORT',
    014,
    'zlomenina stehennej kosti',
    NULL
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('17.3.2015', 'dd.mm.yyyy'),
    4,
    2,
    'CHI',
    'Vykonaný röntgen holennej kosti u pacienta'
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('10.2.2015', 'dd.mm.yyyy'),
    3,
    2,
    'CHI',
    'Vykonaný röntgen stehennej kosti u pacienta'
);

INSERT INTO Vysetrenie VALUES (
    seq_id_vysetrenie.NEXTVAL,
    'Röntgen',
    TO_DATE('29.3.2015', 'dd.mm.yyyy'),
    1,
    3,
    'CHI',
    'Vykonaný röntgen plúc u pacienta'
);

INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Paralen',
    'tableta',
    'paracetamol',
    'Kukuričný škrob predželatínovaný,povidón 30,sodná soľ kroskarmelózy,kyselina stearová',
    'Horúčka,bolesti zubov,hlavy,neuralgie, bolesti svalov alebo kĺbov.',
    'Precitlivenosť na liečivo alebo na ktorúkoľvek z pomocných látok,ťažká hemolytická anémia,ťažké formy hepatálnej insuficiencie,akútna hepatitída.',
    'Podľa potreby v časovom odstupe najmenej 4 hodiny do maximálnej dennej dávky 8 tabliet'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Famciclovir - Teva',
    'Filmom obalená tableta',
    'famciklovir',
    'Mikrokryštalická celulóza E460,Koloidný oxid kremičitý E551,Sodná soľ karboxymetylškrobu',
    'Liečba infekcií pohlavných orgánov herpetickým vírusom,Liečba infekcií kože a slizníc vyvolaných vírusom Herpes zoster.',
    'Precitlivenosť na famciklovir alebo na niektorú z pomocných látok. Precitlivenosť na penciklovir.',
    '250 mg trikrát denne počas 5 dní'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Nalgesin FORTE 550mg',
    'Filmom obalená tableta',
    'sodná soľ naproxénu',
    'Mikrokryštalická celulóza E460,mastenec (E553b),magnéziumstearát (E572)',
    'Tlmí bolesť a zápal a znižuje horúčku,bolesť zubov a hlavy,bolesť v svaloch, kĺboch a chrbtici,prevenciu a liečbu migrény.',
    'Precitlivenosť na salyciláty a iné nesteroidné protizápalové lieky určené na liečbu.',
    '275 mg každých 6 až 8 hodín'
);
INSERT INTO Liek VALUES (
    seq_id_liek.NEXTVAL,
    'Paclitaxel Kabi 6 mg/ml ',
    'infúzny koncentrát',
    'paklitaxel',
    'Etanol(bezvodý),Ricínoleoylmakrogol-glycerol,Kyselina citrónová(bezvodá)',
    'Liečba pacientok s pokročilým ovariálnym karcinómom alebo s reziduálnou chorobou.',
    'Paklitaxel sa nemá používať u pacientov s východiskovým počtom neutrofilov.',
    '20 mg perorálne približne 12 a 6 hodín'
);

INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    1,
    1,
    TO_DATE('22.3.2015', 'dd.mm.yyyy'),
    '3x denne',
    '1 tableta ',
    NULL
);
INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    3,
    1,
    TO_DATE('20.3.2015', 'dd.mm.yyyy'),
    'každých 6 hodín',
    '1 tableta',
    'tabletu užívať po jedle'
);
INSERT INTO Naordinovany_Liek VALUES (
    seq_id_naordinovany_liek.NEXTVAL,
    3,
    3,
    TO_DATE('15.3.2015', 'dd.mm.yyyy'),
    'každých 6 hodín',
    'polovica tablety',
    NULL
);


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
 *  Nastavenie prav pre uzivatela xstrec05
 */
GRANT ALL  ON Lekar TO xstrec05;
GRANT ALL  ON Vysetrenie TO xstrec05;

/*
 *   Materializovany pohlad, vytvara druhy clen timu
 */
CREATE MATERIALIZED VIEW Lekar_view
  NOLOGGING
  CACHE
  BUILD IMMEDIATE
  ENABLE QUERY REWRITE
  AS
    SELECT L.id, L.priezvisko, L.meno, V.nazov, V.datum
      FROM xcerne01.Lekar L JOIN xcerne01.Vysetrenie V ON L.id = V.id_lekar;

SELECT * FROM Lekar_view;

EXPLAIN PLAN FOR
      SELECT L.id, L.priezvisko, L.meno, V.nazov, V.datum
      FROM xcerne01.Lekar L, xcerne01.Vysetrenie V
      WHERE L.id = V.id_lekar;
SELECT plan_table_output from table(dbms_xplan.display(null, null, 'basic'));


ALTER SESSION SET query_rewrite_enabled = true;
EXPLAIN PLAN FOR 
      SELECT L.id, L.priezvisko, L.meno, V.nazov, V.datum
      FROM xcerne01.Lekar L JOIN xcerne01.Vysetrenie V ON L.id = V.id_lekar;
SELECT plan_table_output from table(dbms_xplan.display(null, null, 'basic'));


/*
 *  Vytvorenie indexu a prevedenie EXPLAIN PLANU
 */
EXPLAIN PLAN FOR
  SELECT O.skratka, O.nazov, COUNT(S.id_oddelenie) pocet_sestier
    FROM Oddelenie O INNER JOIN Sestra S ON O.skratka = S.id_oddelenie
    GROUP BY O.skratka, O.nazov;

SELECT plan_table_output from table(dbms_xplan.display);

CREATE INDEX sestra_oddelenie on Sestra(id_oddelenie);

EXPLAIN PLAN FOR
  SELECT O.skratka, O.nazov, COUNT(S.id_oddelenie) pocet_sestier
    FROM Oddelenie O INNER JOIN Sestra S ON O.skratka = S.id_oddelenie
    GROUP BY O.skratka, O.nazov;

SELECT plan_table_output from table(dbms_xplan.display);


/*
 *  Vytvorenie procedury, ktora aktualizuje datum preskolenia sestier na oddeleni
 */
CREATE PROCEDURE aktualizuj_datum_preskolenia(v_oddelenie CHAR, v_datum DATE)
IS
  CURSOR kurzor1 IS
    SELECT meno, priezvisko ,datum_preskolenia
    FROM Sestra
    WHERE id_oddelenie = v_oddelenie
    FOR UPDATE OF datum_preskolenia NOWAIT;
BEGIN
  FOR sestra_rec IN kurzor1 LOOP
    IF v_datum < sestra_rec.datum_preskolenia THEN
      raise_application_error(-20500,'Nespravny datum');
    END IF;
    UPDATE Sestra
    SET datum_preskolenia = v_datum
    WHERE CURRENT OF kurzor1;
  END LOOP;
  COMMIT;
END;
/

SELECT * FROM Sestra;
exec aktualizuj_datum_preskolenia('ORT', TO_DATE('10.5.2015', 'dd.mm.yyyy'));
exec aktualizuj_datum_preskolenia('CHI', TO_DATE('10.5.2013', 'dd.mm.yyyy'));
SELECT * FROM Sestra;


/*
 *  Vytvorenie procedury, ktora vypise kolko dni je pacient aktualne hospitalizovany
 */
CREATE PROCEDURE aktualne_hospitalizovany(rodne_cislo Pacient.rc%TYPE)
IS
  v_datum DATE := sysdate;
  v_zaciatok Hospitalizacia.zaciatok%TYPE;
  v_pocet_dni NUMBER;
BEGIN
  SELECT Hospitalizacia.zaciatok
    INTO v_zaciatok
    FROM Pacient, Hospitalizacia
    WHERE Pacient.rc = rodne_cislo AND Pacient.rc = Hospitalizacia.rc_pacient AND Hospitalizacia.koniec IS NULL;
  v_pocet_dni := TRUNC(v_datum - v_zaciatok);
  dbms_output.put_line(v_pocet_dni);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    dbms_output.put_line('Pacient s rodnym cislom ' || rodne_cislo || ' nie je aktualne hospitalizovany.');
END;
/

SET SERVEROUTPUT ON;
exec aktualne_hospitalizovany('7706240641');
exec aktualne_hospitalizovany('7706240621');


/*
 *  Trigger pre kontrolu rodneho cisla
 */
CREATE OR REPLACE TRIGGER rodne_cislo_trigger
  BEFORE INSERT OR UPDATE OF rc ON Pacient
  FOR EACH ROW
DECLARE
  v_rodne_cislo NUMBER := TO_NUMBER(:NEW.rc);
  v_den NUMBER;
  v_mesiac NUMBER;
  v_rok NUMBER;
BEGIN
  
  IF MOD(v_rodne_cislo, 11) <> 0 THEN
    raise_application_error(-20501,'Rodne cislo nie je delitelne 11');
  END IF;
  
  v_rok := TRUNC(v_rodne_cislo / 100000000);
  v_mesiac := MOD(TRUNC(v_rodne_cislo / 1000000), 100);
  v_den := MOD(TRUNC(v_rodne_cislo / 10000), 100);

  IF v_mesiac <= 0 OR v_mesiac > 12 THEN
    raise_application_error(-20502,'Neplatny mesiac');
  END IF;
  
  IF v_den > 50 THEN
    v_den := v_den - 50;
  END IF;
  
  IF v_den <= 0 THEN
    raise_application_error(-20503,'Neplatny den');
  END IF;
  
  IF v_mesiac = 4 OR v_mesiac = 6 OR v_mesiac = 9 OR v_mesiac = 11 THEN
    IF v_den > 30 THEN
      raise_application_error(-20503,'Neplatny den');
    END IF;
  ELSIF v_mesiac = 2 THEN
    IF MOD(v_rok, 4) = 0 THEN
      IF v_den > 29 THEN
        raise_application_error(-20503,'Neplatny den');
      END IF;
    ELSE
      IF v_den > 28 THEN
        raise_application_error(-20503,'Neplatny den');
      END IF;
    END IF;
  ELSE
    IF v_den > 31 THEN
      raise_application_error(-20503,'Neplatny den');
    END IF;
  END IF;

END;
/

INSERT INTO Pacient VALUES (
    '9413237153',
    'Černek',
    'Martin',
    TO_DATE('16.11.1993', 'dd.mm.yyyy'),
    'Dôvera',
    'Družby',
    54,
    'Piešťany',
    '92101',
    '0915435736'
);


/*
 *  Trigger pre generovanie primarneho kluca sestry
 */
CREATE OR REPLACE TRIGGER nova_sestra_trigger
  BEFORE INSERT ON Sestra
  FOR EACH ROW
BEGIN
  IF :NEW.id IS NULL THEN
    :NEW.id := seq_id_sestra.NEXTVAL;
  END IF;
END;
/

INSERT INTO Sestra VALUES (
    NULL,
    'Lacková',
    'Anna',
    TO_DATE('2.5.1987', 'dd.mm.yyyy'),
    'Hlavná',
    '22',
    'Vrbové',
    '92101',
    '0915156924',
    TO_DATE('2.2.2015', 'dd.mm.yyyy'),
    'CHI'
);

SELECT * FROM Sestra;