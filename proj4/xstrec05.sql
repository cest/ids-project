
/*
 *   Materializovany pohlad
 */
DROP MATERIALIZED VIEW Lekar_view;

--create materialized view log on xcerne01.Lekar with rowid;

CREATE MATERIALIZED VIEW Lekar_view
  NOLOGGING
  CACHE
  BUILD IMMEDIATE
  ENABLE QUERY REWRITE
  AS
    SELECT L.id, L.priezvisko, L.meno, L.ulica, L.cislo, L.mesto, L.psc
      FROM xcerne01.Lekar L;


EXPLAIN PLAN FOR SELECT L.*
      FROM xcerne01.Lekar L;
SELECT plan_table_output from table(dbms_xplan.display(null, null, 'basic'));


ALTER SESSION SET query_rewrite_enabled = true;
EXPLAIN PLAN FOR 
      SELECT L.id, L.priezvisko, L.meno, L.ulica, L.cislo, L.mesto, L.psc
      FROM xcerne01.Lekar L;
SELECT plan_table_output from table(dbms_xplan.display(null, null, 'basic'));
