### INFO

* **2.3. až 6.3.2015** – Odevzdání a obhajoba 1. části projektu (datový model a model případů užití). Ve WISu je nutno se na termín obhajoby (vždy jeden člen za tým) přihlásit do 1.3.2015 a dále odevzdat (nahrát) řešení do WISu před termínem obhajoby. Na obhajobu si s sebou přineste vytištěné řešení (modely), které budete při obhajobě popisovat.
* **do 29.3.2015** – Odevzdání 2. části projektu (SQL skript pro vytvoření základních objektů schématu databáze).
* **do 12.4.2015** – Odevzdání 3. části projektu (SQL skript s několika dotazy SELECT).
* **do 4.5.2015** – Odevzdání 4. a 5. části projektu (SQL skript pro vytvoření pokročilých objektů schématu databáze a Dokumentace popisující finální schéma databáze).
* **5.5. až 15.5.2015** – Závěrečná obhajoba projektu.
Na obhajobu je nutno se přihlásit do 3.5.2015 a ve zvolený termín se dostavit obhájit řešení. Pozor, řešení poslední části projektu se odevzdává v termín dle předchozí odrážky, nikoliv až těsně před obhajobou.

__Viacej informácií__ [TU](https://www.fit.vutbr.cz/study/courses/IDS/private/projekt.xhtml).

### Zadanie
Navrhnite IS malej nemocnice, ktorý by poskytoval základné údaje o lekároch,
sestrách či pacientoch, ktorí sú a boli hospitalizovaní v nemocnici. IS uchováva
informácie o všetkých týchto hospitalizáciach, pričom pacient môže byť v jeden čas
hospitalizovaný iba na jednom oddelení nemocnice. Pri každej hospitalizácií je mu
určený jeho ošetrujúci lekár. Lekári môžu pracovať na viacerých oddeleniach
zároveň. Na každom oddelení má lekár určitý úväzok, telefón atď., zatiaľ čo sestry
pracujú iba na jednom oddelení. V rámci pobytu v nemocnici môže pacienť
podstúpiť rôzne vyšetrenia, ktoré boli vykonané na určitom oddelení v stanovenom
čase a vykonal ich určitý lekár, ktorý tiež zapisuje výsledky vyšetrení do IS. Ďalej
mu môžu byť podávané rôzne lieky, každé podávanie lieku má určité detaily (kedy
sa podáva, koľkokrát apod.).V systému sú uložené aj všeobecné informácie o
liekoch (názov, účinná látka, sila lieku, kontraindikácie atď.), aby si lekár mohol
skontrolovať správnosť naordinovaného dávkovania.

### 4. časť
SQL skript v poslední části projektu musí obsahovat vše z následujících
* vytvoření alespoň dvou netriviálních databázových triggerů vč. jejich předvedení, z toho právě jeden trigger pro automatické generování hotnot primárního klíče nějaké tabulky ze sekvence (např. pokud bude při vkládání záznamů do dané tabulky hodnota primárního klíče nedefinována, tj. NULL),
* vytvoření alespoň dvou netriviálních uložených procedur vč. jejich předvedení, ve kterých se musí (dohromady) vyskytovat alespoň jednou kurzor, ošetření výjimek a použití proměnné s datovým typem odkazujícím se na řádek či typ sloupce tabulky (table_name.column_name%TYPE nebo table_name%ROWTYPE),
* explicitní vytvoření alespoň jednoho indexu tak, aby pomohl optimalizovat zpracování dotazů, přičemž musí být uveden také příslušný dotaz, na který má index vliv, a v dokumentaci popsán způsob využití indexu v tomto dotazy (toto lze zkombinovat s EXPLAIN PLAN, vizte dále),
* alespoň jedno použití EXPLAIN PLAN pro výpis plánu provedení databazového dotazu se spojením alespoň dvou tabulek, agregační funkcí a klauzulí GROUP BY, přičemž v dokumentaci musí být srozumitelně popsáno, jak proběhne dle toho výpisu plánu provedení dotazu, vč. objasnění použitých prostředků pro jeho urychlení (např. použití indexu, druhu spojení, atp.), a dále musí být navrnut způsob, jak konkrétně by bylo možné dotaz dále urychlit (např. zavedením nového indexu), navržený způsob proveden (např. vytvořen index), zopakován EXPLAIN PLAN a jeho výsledek porovnán s výsledkem před provedením navrženého způsobu urychlení,
* definici přístupových práv k databázovým objektům pro druhého člena týmu,
* vytvořen alespoň jeden materializovaný pohled patřící druhému členu týmu a používající tabulky definované prvním členem týmu (nutno mít již definována přístupová práva), vč. SQL příkazů/dotazů ukazujících, jak materializovaný pohled funguje,